Source: openfpgaloader
Section: electronics
Priority: optional
Maintainer: Debian Electronics Team <pkg-electronics-devel@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 12),
 libftdi1-dev,
 libhidapi-dev,
 libudev-dev,
 libusb-dev,
 zlib1g-dev,
 libgpiod-dev,
 cmake,
 pkg-config
Standards-Version: 4.6.2
Homepage: https://github.com/trabucayre/openFPGALoader
Vcs-Browser: https://salsa.debian.org/electronics-team/openfpgaloader
Vcs-Git: https://salsa.debian.org/electronics-team/openfpgaloader.git

Package: openfpgaloader
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Suggests: yosys
Description: Universal utility for programming FPGAs
 FPGAs are software-programmable reconfigurable circuits that when
 may implement arbitrary logics, be it to interface to other hardware
 as some sort of glue logic or to even outsource computations from your
 CPU as an accelerator. Even small FPGAs are today sufficiently capable
 to simulate a CPU.
 .
 This package knows how to bring the firmware that is built on a regular
 computer, e.g., with yosys and assorted tools, to the FPGA board, such
 that such a bitstream is then executed.
 .
 OpenFPGALoader is compatible with many boards, cables and FPGA from
 major manufacturers (Xilinx, Altera/Intel, Lattice, Gowin, Efinix,
 Anlogic, Cologne Chip).
